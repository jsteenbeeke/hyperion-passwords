package com.jeroensteenbeeke.hyperion.passwordpolicy;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;

public class ListFileAnalyzerTest {
	@Test
	public void readInvalidFile() {
		assertThrows(IOException.class, () -> {
			try {
				ListFileAnalyzer.fileContains("yolo.txt", "YOLO"::equals);
			} catch (RuntimeException e) {
				throw e.getCause();
			}
		});
	}
}
