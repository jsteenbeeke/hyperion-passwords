package com.jeroensteenbeeke.hyperion.passwordpolicy;

import org.junit.jupiter.api.Test;

import static com.jeroensteenbeeke.hyperion.passwordpolicy.PasswordPolicy.isCommonPassword;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CommonPasswordTest {
	@Test
	public void testPassword() {
		assertCommon(new char[] {'p','a','s','s','w','o','r','d'});
		assertCommon(new char[] {'P','a','s','s','w','o','r','d'});
		assertCommon(new char[] {'v','j','h','t','0','0','8'});
		assertUncommon(new char[] {'B','V','l','x','C','z','x','W','s','o'});
	}

	private void assertCommon(char[] password) {
		assertTrue(isCommonPassword(password), String.format("Password '%s' expected restricted but found approved", new String(password)));
	}

	private void assertUncommon(char[] password) {
		assertFalse(isCommonPassword(password), String.format("Password '%s' expected approved but found restricted", new String(password)));
	}
}
