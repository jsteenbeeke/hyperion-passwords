package com.jeroensteenbeeke.hyperion.passwordpolicy;

import org.junit.jupiter.api.Test;

import static com.jeroensteenbeeke.hyperion.passwordpolicy.PasswordPolicy.getTopology;
import static com.jeroensteenbeeke.hyperion.passwordpolicy.PasswordPolicy.isCommonTopology;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class PasswordTopologyTest {
	@Test
	public void testTopologies() {
		assertTopology(new char[] { 'p','a','s','s','w','o','r','d' });
		assertTopology(new char[] { 'P','a','s','s','w','o','r','d' });
		assertTopology(new char[] { '1','3','3','7' });

		assertNonTopology(new char[] { 's','a','d','g','h','j','3','q','4','7','8','7','6','7','8','d','g','s','h','j','d','s','f','g','h','j'});
		assertNonTopology(new char[] { 's','p','0','0','n','$','$','_','_','_','_','_','_','_' });

	}

	private void assertTopology(char[] password) {
		assertTrue(isCommonTopology(password),
				String.format("Password '%s' (topology '%s') expected restricted but found approved", new String(password), getTopology(password)));
	}

	private void assertNonTopology(char[] password) {
		assertFalse(isCommonTopology(password),
				String.format("Password '%s' (topology '%s') expected approved but found restricted", new String(password), getTopology(password)));
	}
}
