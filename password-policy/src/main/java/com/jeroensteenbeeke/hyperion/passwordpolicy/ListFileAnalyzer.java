package com.jeroensteenbeeke.hyperion.passwordpolicy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.function.Predicate;

class ListFileAnalyzer {
	static boolean fileContains(String inputFile, Predicate<String> lineMatcher) {
		try (InputStream in = ListFileAnalyzer.class.getResourceAsStream(inputFile)) {
			if (in == null) {
				throw new IOException("Invalid file");
			}

			try (InputStreamReader isr = new InputStreamReader(in);
			 BufferedReader br = new BufferedReader(isr)) {
				String pw;
				while ((pw = br.readLine()) != null) {
					if (lineMatcher.test(pw)) {
						return true;
					}
				}

				return false;
			}
		} catch (IOException e) {
			throw new RuntimeException("Cannot read file "+ inputFile, e);
		}
	}
}
