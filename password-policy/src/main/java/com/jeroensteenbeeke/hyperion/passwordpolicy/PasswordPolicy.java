package com.jeroensteenbeeke.hyperion.passwordpolicy;

import java.util.Arrays;

/**
 * Contains convenience methods to check passwords against a number of rules
 */
public class PasswordPolicy {
	/**
	 * Determines whether or not a password is common, by comparing it against the top million
	 * passwords found on https://github.com/danielmiessler/SecLists/tree/master/Passwords/Common-Credentials
	 *
	 * @param password The password to check
	 * @return {@code true} if the password was on the list (=bad), {@code false} otherwise
	 */
	public static boolean isCommonPassword(char[] password) {
		return ListFileAnalyzer.fileContains("10-million-password-list-top-1000000.txt",
				line -> Arrays.equals(line.toCharArray(), password));
	}

	/**
	 * Determines whether or not the given password is a common topology, by comparing it against a list
	 * of pregenerated topologies (based on both the top password list and sample lists from Pathwell
	 *
	 * @param password The password to verify
	 * @return {@code true} if the password's topology is common (=bad), {@code false} otherwise
	 */
	public static boolean isCommonTopology(char[] password) {
		return ListFileAnalyzer.fileContains("topologies.txt", getTopology(password)::equals);
	}

	/**
	 * Determines the password's topology
	 *
	 * @param password The password to check
	 * @return The determined topology (u=uppercase,l=lowercase,d=digit,s=symbol)
	 */
	static String getTopology(char[] password) {
		StringBuilder topology = new StringBuilder();

		for (char c : password) {
			if (Character.isUpperCase(c)) {
				topology.append('u');
			} else if (Character.isLowerCase(c)) {
				topology.append('l');
			} else if (Character.isDigit(c)) {
				topology.append('d');
			} else {
				topology.append('s');
			}

		}

		return topology.toString();
	}
}
