@Library(value = 'jenkins-shared-library@main', changelog = false)
import com.jeroensteenbeeke.hyperion.*

Hyperion hyperion = new Hyperion(this)
def pollInterval = hyperion.scmPollInterval()

pipeline {
    agent {
        docker {
            image 'registry.jeroensteenbeeke.nl/maven:latest'
            label 'docker'
        }
    }

    triggers {
        pollSCM(pollInterval)
        upstream(upstreamProjects: 'hyperion/main', threshold: hudson.model.Result.SUCCESS)
    }

    options {
        buildDiscarder(logRotator(numToKeepStr: '5'))
        disableConcurrentBuilds()
    }

    environment {
        MAVEN_DEPLOY_USER = credentials('MAVEN_DEPLOY_USER')
        MAVEN_DEPLOY_PASSWORD = credentials('MAVEN_DEPLOY_PASSWORD')
    }


    stages {
        stage('Maven') {
            steps {
                sh 'mvn -B -ntp -U clean verify install -P-disable-slow-tests'
            }
        }
        stage('Deploy') {
            when {
                expression {
                    (currentBuild.result == 'SUCCESS' || currentBuild.result == null)
                }
            }

            steps {
                mavenDeploy deployUser: env.MAVEN_DEPLOY_USER,
                        deployPassword: env.MAVEN_DEPLOY_PASSWORD
            }
        }
    }

    post {
        always {
            script {
                if (currentBuild.result == null) {
                    currentBuild.result = 'SUCCESS'
                }
            }

            step([$class                  : 'Mailer',
                  notifyEveryUnstableBuild: true,
                  sendToIndividuals       : true,
                  recipients              : 'j.steenbeeke@gmail.com'
            ])

            recordIssues enabledForFailure: true, tools: [mavenConsole(), java(), javaDoc()]
            junit allowEmptyResults: true, testResults: '**/target/test-reports/*.xml'
            recordIssues enabledForFailure: true, tool: checkStyle(pattern: '**/target/checkstyle-result.xml')
            recordIssues enabledForFailure: true, tool: spotBugs()

        }
    }

}
