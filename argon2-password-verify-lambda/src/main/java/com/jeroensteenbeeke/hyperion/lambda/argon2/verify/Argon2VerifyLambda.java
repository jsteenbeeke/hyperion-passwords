package com.jeroensteenbeeke.hyperion.lambda.argon2.verify;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Argon2VerifyLambda implements RequestHandler<Argon2VerifyRequest, Boolean> {
	private static final int MEMORY_MARGIN = 1024 * 32;

	private static final Pattern ARGON2_HASH = Pattern.compile("^\\$argon2([di]*)\\$v=(\\d+)\\$m=(\\d+),t=(\\d+),p=(\\d+)\\$([A-Za-z0-9+/=]+)\\$([A-Za-z0-9+/=]*)$");

	@Override
	public Boolean handleRequest(Argon2VerifyRequest request, Context context) {
		Matcher matcher = ARGON2_HASH.matcher(request.getHash());

		if (!matcher.matches()) {
			throw new IllegalArgumentException("Invalid Argon2 hash, matches: " + matcher
					.results()
					.map(mr -> String.format("%d-%d", mr.start(), mr.end()))
					.collect(Collectors.joining(", ")));
		}

		int iterations = Integer.parseInt(matcher.group(4));
		int threads = Integer.parseInt(matcher.group(5));
		int memoryInKB = Integer.parseInt(matcher.group(3));

		verifyRange("iterations", iterations, 1, null);
		verifyRange("threads", threads, 1, Runtime.getRuntime().availableProcessors());
		verifyRange("memoryInKB", memoryInKB, 128*1024, context.getMemoryLimitInMB() * 1024 - MEMORY_MARGIN);

		Argon2 argon2 = Argon2Factory.create(Argon2Factory.Argon2Types.ARGON2id);
		char[] password = request.getPassword();

		try {
			return argon2.verify(request.getHash(), password);
		} finally {
			argon2.wipeArray(password);
		}
	}

	private void verifyRange(String label, int value, Integer min, Integer max) {
		if (value == 0) {
			throw new IllegalArgumentException(String.format("Missing parameter '%s'", label));
		}

		if (min != null && value < min) {
			throw new IllegalArgumentException(String.format("Invalid value %d for parameter '%s', should be at least %d", value, label, min));
		}

		if (max != null && value > max) {
			throw new IllegalArgumentException(String.format("Invalid value %d for parameter '%s', should be at most %d", value, label, max));
		}

	}
}
