package com.jeroensteenbeeke.hyperion.lambda.argon2.verify;

public class Argon2VerifyRequest {
	private String hash;

	private char[] password;

	public Argon2VerifyRequest(String hash, char[] password) {
		this.hash = hash;
		this.password = password;
	}

	public Argon2VerifyRequest() {
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

	public char[] getPassword() {
		return password;
	}

	public void setPassword(char[] password) {
		this.password = password;
	}
}
