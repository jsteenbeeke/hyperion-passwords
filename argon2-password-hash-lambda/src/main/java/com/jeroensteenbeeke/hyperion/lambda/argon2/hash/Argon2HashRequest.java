package com.jeroensteenbeeke.hyperion.lambda.argon2.hash;

public class Argon2HashRequest {
	private char[] password;

	private int iterations;

	private int threads;

	private int memoryInKb;

	public Argon2HashRequest(char[] password, int iterations, int threads, int memoryInKb) {
		this.password = password;
		this.iterations = iterations;
		this.threads = threads;
		this.memoryInKb = memoryInKb;
	}

	public Argon2HashRequest() {
	}

	public char[] getPassword() {
		return password;
	}

	public void setPassword(char[] password) {
		this.password = password;
	}

	public int getIterations() {
		return iterations;
	}

	public void setIterations(int iterations) {
		this.iterations = iterations;
	}

	public int getThreads() {
		return threads;
	}

	public void setThreads(int threads) {
		this.threads = threads;
	}

	public int getMemoryInKb() {
		return memoryInKb;
	}

	public void setMemoryInKb(int memoryInKb) {
		this.memoryInKb = memoryInKb;
	}
}
