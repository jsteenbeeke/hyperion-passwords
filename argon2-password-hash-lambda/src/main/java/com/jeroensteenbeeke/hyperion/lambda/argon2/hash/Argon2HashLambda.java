package com.jeroensteenbeeke.hyperion.lambda.argon2.hash;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;

import java.nio.charset.StandardCharsets;

public class Argon2HashLambda implements RequestHandler<Argon2HashRequest, String> {
	private static final int MEMORY_MARGIN = 1024 * 32;

	@Override
	public String handleRequest(Argon2HashRequest request, Context context) {
		int iterations = request.getIterations();
		int threads = request.getThreads();
		int memoryInKB = request.getMemoryInKb();

		verifyRange("iterations", iterations, 1, null);
		verifyRange("threads", threads, 1, Runtime.getRuntime().availableProcessors());
		verifyRange("memoryInKB", memoryInKB, 128*1024, context.getMemoryLimitInMB() * 1024 - MEMORY_MARGIN);

		char[] password = request.getPassword();

		Argon2 argon2 = Argon2Factory.create(Argon2Factory.Argon2Types.ARGON2id);

		try {
			return argon2.hash(iterations, memoryInKB, threads, password, StandardCharsets.UTF_8);
		} finally {
			argon2.wipeArray(password);
		}
	}

	private void verifyRange(String label, int value, Integer min, Integer max) {
		if (value == 0) {
			throw new IllegalArgumentException(String.format("Missing parameter '%s'", label));
		}

		if (min != null && value < min) {
			throw new IllegalArgumentException(String.format("Invalid value %d for parameter '%s', should be at least %d", value, label, min));
		}

		if (max != null && value > max) {
			throw new IllegalArgumentException(String.format("Invalid value %d for parameter '%s', should be at most %d", value, label, max));
		}

	}
}
