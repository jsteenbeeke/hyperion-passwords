package com.jeroensteenbeeke.hyperion.solstice.argon2;

import io.vavr.control.Either;
import org.jetbrains.annotations.NotNull;

/**
 * Facade for Argon2 hashing functionality
 */
public interface Argon2HashProvider {
	/**
	 * Generates an Argon2 hash for the given password
	 * @param password The password to hash
	 * @return Either the password hash (right) or the reason it could not be created (left)
	 */
	@NotNull
	Either<String,String> generateHash(char[] password);

	/**
	 * Checks whether the given hash represents the given password. This is done by hashing the password
	 * again using the settings stored in the hash.
	 *
	 * @param password The password
	 * @param hash The hash to check against
	 * @return {@code true} if the password is correct, {@code false} if it is not (both right). If an error occurs, its
	 * description is returned (left).
	 */
	@NotNull
	Either<String,Boolean> verifyHash(char[] password, String hash);
}
