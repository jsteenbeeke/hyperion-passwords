package com.jeroensteenbeeke.hyperion.solstice.argon2;

import com.jeroensteenbeeke.hyperion.password.argon2.Argon2PasswordHasher;
import io.vavr.control.Either;
import org.jetbrains.annotations.NotNull;

/**
 * Argon2 hash provider implementation that computes the hash locally
 */
public class LocalArgon2HashProvider implements Argon2HashProvider {
	private final Argon2Config argon2Config;

	LocalArgon2HashProvider(@NotNull Argon2Config argon2Config) {
		this.argon2Config = argon2Config;
	}

	@Override
	@NotNull
	public Either<String, String> generateHash(@NotNull char[] password) {
		return Either.right(Argon2PasswordHasher
									.hashNewPassword(password)
									.withIterations(argon2Config.getIterations())
									.withMemoryInKiB(argon2Config.getMemoryInKb())
									.withParallelism(argon2Config.getParallelism()));
	}

	@Override
	@NotNull
	public Either<String, Boolean> verifyHash(@NotNull char[] password, @NotNull String hash) {
		return Either.right(Argon2PasswordHasher.checkExistingPassword(password).withHash(hash));
	}
}
