package com.jeroensteenbeeke.hyperion.solstice.argon2;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Spring configuration class that sets up Argon2 hashing based on user-defined properties. Can use
 * both local and remote hashing (using AWS Lambda, see sibling projects).
 * <p>
 * Will default to sensible defaults if no hashing parameters are set up.
 * <p>
 * The following properties can be defined:
 * <ul>
 *     <li>{@code argon2.iterations} (default: 2)</li>
 *     <li>{@code argon2.parallelism} (default: 2)</li>
 *     <li>{@code argon2.memoryInKb} (default: 131072, i.e. 128MB)</li>
 * </ul>
 * <p>
 * To configure AWS Lambda, the following properties are required:
 * <ul>
 *     <li>{@code amazon.argon2.key} - The Amazon key to use for authentication</li>
 *     <li>{@code amazon.argon2.secret} - The secret belonging to above key</li>
 *     <li>{@code amazon.argon2.region} - The region where the lambda can be invoked</li>
 *     <li>{@code amazon.argon2.hash.arn} - The ARN of the hashing lambda (use project {@code hyperion-argon2-create-hash-lambda})</li>
 *     <li>{@code amazon.argon2.verify.arn} - The ARN of the verification lambda (use project {@code argon2-password-verify-lambda})</li>
 * </ul>
 * <p>
 * To use the AWS lambda functionality you will need to build the sibling projects {@code hyperion-argon2-create-hash-lambda} and {@code argon2-password-verify-lambda}
 * and deploy them to Amazon ECR (package the projects, create a repository for each project, then follow the instructions for pushing the image as
 * displayed on ECR).
 * <p>
 * Once pushed, you can create lambda functions for the images, and put the ARN of the two lambda's in the application config.
 */
@Configuration
public class Argon2Feature {
	private static final Logger log = LoggerFactory.getLogger(Argon2Feature.class);

	/**
	 * Creates an Argon2 configuration based on the given specification. If the keys specified are
	 * not defined, will default to OWASP recommended settings dated 28th of February 2019.
	 *
	 * @param iterations  The number of iterations to run
	 * @param parallelism The number of threads to use
	 * @param memoryInKb  The memory limit in kilobytes for the hashing algorithm
	 * @return An Argon2 configuration
	 */
	@Bean
	@NotNull
	public Argon2Config createConfig(@Value("${argon2.iterations:2}") int iterations,
									 @Value("${argon2.parallelism:4}") int parallelism,
									 @Value("${argon2.memoryInKb:131072}") int memoryInKb) {
		return new Argon2Config(iterations, parallelism, memoryInKb);
	}

	/**
	 * Creates a new Argon2 provider
	 * @param argon2Config The configuration bean for hashing settings
	 * @param key The Amazon AWS key
	 * @param secret The Amazon AWS secret
	 * @param hashARN The ARN of the hashing lambda
	 * @param verifyARN The ARN of the verification lambda
	 * @param region The region where the lambda's are hosted
	 * @return An implementation of Argon2HashProvider
	 */
	@Bean
	@NotNull
	public Argon2HashProvider argon2Provider(
			Argon2Config argon2Config,
			@Value("${amazon.argon2.key:}") String key,
			@Value("${amazon.argon2.secret:}") String secret,
			@Value("${amazon.argon2.hash.arn:}") String hashARN,
			@Value("${amazon.argon2.verify.arn:}") String verifyARN,
			@Value("${amazon.argon2.region:}") String region
	) {
		if (key.isBlank() || secret.isBlank() || hashARN.isBlank() || verifyARN.isBlank() || region.isBlank()) {
			log.warn("Missing properties for AWS Lambda Argon2 hashing. Defaulting to local hashing");
			log.info("The following settings were missing:");
			if (key.isBlank()) {
				log.info(" - amazon.argon2.key");
			}
			if (secret.isBlank()) {
				log.info(" - amazon.argon2.secret");
			}
			if (hashARN.isBlank()) {
				log.info(" - amazon.argon2.hash.arn");
			}
			if (verifyARN.isBlank()) {
				log.info(" - amazon.argon2.verify.arn");
			}
			if (region.isBlank()) {
				log.info(" - amazon.argon2.region");
			}

			return new LocalArgon2HashProvider(argon2Config);
		} else {
			log.info("Using AWS Lambda for Argon2 hashing");
			return new AWSLambdaArgon2HashProvider(argon2Config, key, secret, hashARN, verifyARN, region);
		}
	}
}
