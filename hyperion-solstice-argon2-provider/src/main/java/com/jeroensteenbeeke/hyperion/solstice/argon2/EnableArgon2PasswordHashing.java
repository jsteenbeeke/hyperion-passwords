package com.jeroensteenbeeke.hyperion.solstice.argon2;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Spring configuration annotation that triggers loading of Argon2 classes
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import(Argon2Feature.class)
public @interface EnableArgon2PasswordHashing {
}
