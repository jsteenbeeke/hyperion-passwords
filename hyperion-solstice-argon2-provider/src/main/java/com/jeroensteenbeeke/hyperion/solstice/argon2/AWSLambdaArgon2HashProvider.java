package com.jeroensteenbeeke.hyperion.solstice.argon2;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.control.Either;
import io.vavr.control.Try;
import org.jetbrains.annotations.NotNull;

import java.nio.charset.StandardCharsets;

/**
 * Hash provider implementation that uses AWS Lambda for the hashing
 */
public class AWSLambdaArgon2HashProvider implements Argon2HashProvider {
	private final Argon2Config argon2Config;

	private final String hashARN;

	private final String verifyARN;

	private final AWSLambda awsLambda;

	private final ObjectMapper objectMapper;

	AWSLambdaArgon2HashProvider(
			@NotNull Argon2Config argon2Config, @NotNull String key, @NotNull String secret, @NotNull String hashARN,
			@NotNull String verifyARN, @NotNull String region) {
		this.argon2Config = argon2Config;
		this.awsLambda = AWSLambdaClientBuilder.standard()
				.withCredentials(new AWSStaticCredentialsProvider(
						new BasicAWSCredentials(key, secret)
				))
				.withRegion(region).build();
		this.hashARN = hashARN;
		this.verifyARN = verifyARN;
		this.objectMapper = new ObjectMapper();
	}

	@Override
	@NotNull
	public Either<String, String> generateHash(char[] password) {
		return createHashPayload(password, objectMapper, argon2Config).flatMap(payload -> {
			InvokeRequest invokeRequest = new InvokeRequest()
					.withFunctionName(hashARN)
					.withPayload(payload);
			InvokeResult result = awsLambda.invoke(invokeRequest);

			if (result.getStatusCode() == 200) {
				String rawResult = new String(result.getPayload().array(), StandardCharsets.UTF_8);

				// Result is a JSON string, remove the leading and trailing quote
				return Try
						.of(() -> objectMapper.readValue(rawResult, String.class))
						.toEither()
						.mapLeft(Throwable::getMessage);
			}


			return Either.left(result.getFunctionError());
		});
	}

	@Override
	@NotNull
	public Either<String, Boolean> verifyHash(char[] password, String hash) {
		return createVerifyPayload(password, hash, objectMapper).flatMap(payload -> {
			InvokeRequest invokeRequest = new InvokeRequest()
					.withFunctionName(verifyARN)
					.withPayload(payload);

			InvokeResult result = awsLambda.invoke(invokeRequest);

			if (result.getStatusCode() == 200) {
				return Try
						.of(() -> objectMapper.readValue(result.getPayload().array(), String.class))
						.toEither()
						.map(Boolean::parseBoolean)
						.mapLeft(Throwable::getMessage);
			}

			return Either.left(result.getFunctionError());
		});
	}

	@NotNull
	static Either<String, String> createHashPayload(@NotNull char[] password, @NotNull ObjectMapper objectMapper, @NotNull Argon2Config config) {
		try {
			return Either.right(objectMapper.writeValueAsString(new Argon2HashRequest(password, config
					.getIterations(), config.getParallelism(), config
					.getMemoryInKb())));
		} catch (JsonProcessingException e) {
			return Either.left(e.getMessage());
		}
	}

	@NotNull
	static Either<String, String> createVerifyPayload(@NotNull char[] password, @NotNull String hash, @NotNull ObjectMapper objectMapper) {
		try {
			return Either.right(objectMapper.writeValueAsString(new Argon2VerifyRequest(hash, password)));
		} catch (JsonProcessingException e) {
			return Either.left(e.getMessage());
		}
	}

	private static class Argon2HashRequest {
		private final char[] password;

		private final int iterations;

		private final int threads;

		private final int memoryInKb;

		public Argon2HashRequest(@NotNull char[] password, int iterations, int threads, int memoryInKb) {
			this.password = password;
			this.iterations = iterations;
			this.threads = threads;
			this.memoryInKb = memoryInKb;
		}

		@NotNull
		public char[] getPassword() {
			return password;
		}

		public int getIterations() {
			return iterations;
		}

		public int getThreads() {
			return threads;
		}

		public int getMemoryInKb() {
			return memoryInKb;
		}
	}

	private static class Argon2VerifyRequest {
		private final String hash;

		private final char[] password;

		public Argon2VerifyRequest(@NotNull String hash, @NotNull char[] password) {
			this.hash = hash;
			this.password = password;
		}

		@NotNull
		public String getHash() {
			return hash;
		}

		@NotNull
		public char[] getPassword() {
			return password;
		}

	}
}
