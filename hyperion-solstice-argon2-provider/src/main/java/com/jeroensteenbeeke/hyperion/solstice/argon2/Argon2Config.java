package com.jeroensteenbeeke.hyperion.solstice.argon2;

/**
 * Configuration bean for Argon2 hashing settings
 */
public class Argon2Config {
	private final int iterations;

	private final int parallelism;

	private final int memoryInKb;

	Argon2Config(int iterations, int parallelism, int memoryInKb) {
		this.iterations = iterations;
		this.parallelism = parallelism;
		this.memoryInKb = memoryInKb;
	}

	/**
	 * The number of iterations
	 * @return The number of iterations
	 */
	public int getIterations() {
		return iterations;
	}

	/**
	 * The number of threads to use
	 * @return The number of threads to use
	 */
	public int getParallelism() {
		return parallelism;
	}

	/**
	 * The amount of memory in KB to use
	 * @return The amount of memory to use
	 */
	public int getMemoryInKb() {
		return memoryInKb;
	}
}
