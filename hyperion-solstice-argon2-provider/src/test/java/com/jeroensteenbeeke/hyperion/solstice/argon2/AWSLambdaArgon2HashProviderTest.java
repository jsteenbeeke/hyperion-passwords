package com.jeroensteenbeeke.hyperion.solstice.argon2;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.vavr.control.Either;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class AWSLambdaArgon2HashProviderTest {

	@Test
	public void hash_payload_should_be_created() {
		Either<String, String> payload =
				AWSLambdaArgon2HashProvider.createHashPayload("je moeder".toCharArray(), new ObjectMapper(), new Argon2Config(1, 1, 1));

		assertThat(payload.isLeft(), equalTo(false));
		assertThat(payload.isRight(), equalTo(true));
		assertThat(payload.get(), equalTo("{\"password\":\"je moeder\",\"iterations\":1,\"threads\":1,\"memoryInKb\":1}"));
	}

	@Test
	public void json_injection_should_be_avoided() {
		Either<String, String> payload =
				AWSLambdaArgon2HashProvider.createHashPayload("je moeder\", \"hey_amazon_i_am_malicious_json\": \"5".toCharArray(), new ObjectMapper(), new Argon2Config(1, 1, 1));

		assertThat(payload.isLeft(), equalTo(false));
		assertThat(payload.isRight(), equalTo(true));
		assertThat(payload.get(), equalTo("{\"password\":\"je moeder\\\", \\\"hey_amazon_i_am_malicious_json\\\": \\\"5\",\"iterations\":1,\"threads\":1,\"memoryInKb\":1}"));
	}

	@Test
	public void verify_payload_should_be_created() {
		Either<String, String> payload =
				AWSLambdaArgon2HashProvider.createVerifyPayload("je moeder".toCharArray(), "i_am_pretending_to_be_a_hash", new ObjectMapper());

		assertThat(payload.isLeft(), equalTo(false));
		assertThat(payload.isRight(), equalTo(true));
		assertThat(payload.get(), equalTo("{\"hash\":\"i_am_pretending_to_be_a_hash\",\"password\":\"je moeder\"}"));
	}
}