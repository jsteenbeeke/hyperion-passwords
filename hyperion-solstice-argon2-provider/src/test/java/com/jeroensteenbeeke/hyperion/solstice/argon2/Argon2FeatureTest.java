package com.jeroensteenbeeke.hyperion.solstice.argon2;

import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

@Configuration
@EnableArgon2PasswordHashing
@PropertySource("classpath:argon2-test-${argon2settings}.properties")
public class Argon2FeatureTest {

	@Test
	public void empty_config_uses_default_values() {
		System.setProperty("argon2settings", "empty");

		AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(Argon2FeatureTest.class);

		Argon2Config argon2Config = annotationConfigApplicationContext.getBean(Argon2Config.class);

		assertThat(argon2Config, notNullValue());
		assertThat(argon2Config.getIterations(), equalTo(2));
		assertThat(argon2Config.getParallelism(), equalTo(4));
		assertThat(argon2Config.getMemoryInKb(), equalTo(131072));
	}

	@Test
	public void partial_config_mixes_user_and_default_values() {
		System.setProperty("argon2settings", "partial");

		AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(Argon2FeatureTest.class);

		Argon2Config argon2Config = annotationConfigApplicationContext.getBean(Argon2Config.class);

		assertThat(argon2Config, notNullValue());
		assertThat(argon2Config.getIterations(), equalTo(2));
		assertThat(argon2Config.getParallelism(), equalTo(2));
		assertThat(argon2Config.getMemoryInKb(), equalTo(15360));
	}

	@Test
	public void full_config_uses_only_user_values() {
		System.setProperty("argon2settings", "full");

		AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(Argon2FeatureTest.class);

		Argon2Config argon2Config = annotationConfigApplicationContext.getBean(Argon2Config.class);

		assertThat(argon2Config, notNullValue());
		assertThat(argon2Config.getIterations(), equalTo(5));
		assertThat(argon2Config.getParallelism(), equalTo(2));
		assertThat(argon2Config.getMemoryInKb(), equalTo(15360));
	}

	@Test
	public void empty_aws_config_yields_local_implementation() {
		System.setProperty("argon2settings", "empty");

		AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(Argon2FeatureTest.class);

		Argon2HashProvider hashProvider = annotationConfigApplicationContext.getBean(Argon2HashProvider.class);

		assertThat(hashProvider, notNullValue());
		assertThat(hashProvider, is(instanceOf(LocalArgon2HashProvider.class)));
	}

	@Test
	public void partial_aws_config_yields_local_implementation() {
		System.setProperty("argon2settings", "partial-aws");

		AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(Argon2FeatureTest.class);

		Argon2HashProvider hashProvider = annotationConfigApplicationContext.getBean(Argon2HashProvider.class);

		assertThat(hashProvider, notNullValue());
		assertThat(hashProvider, is(instanceOf(LocalArgon2HashProvider.class)));
	}

	@Test
	public void full_aws_config_yields_aws_implementation() {
		System.setProperty("argon2settings", "full-aws");

		AnnotationConfigApplicationContext annotationConfigApplicationContext = new AnnotationConfigApplicationContext(Argon2FeatureTest.class);

		Argon2HashProvider hashProvider = annotationConfigApplicationContext.getBean(Argon2HashProvider.class);

		assertThat(hashProvider, notNullValue());
		assertThat(hashProvider, is(instanceOf(AWSLambdaArgon2HashProvider.class)));
	}
}